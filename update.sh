#!/bin/bash

cd collection-files
cp ~/.nanorc .
cp ~/.bashrc .
cp ~/.inputrc .
cp ~/.irbrc .
cp ~/.config/fish/functions/fish_prompt.fish .

ln --relative --force -s .nanorc ~/.nanorc
ln --relative --force -s .bashrc ~/.bashrc
ln --relative --force -s .inputrc ~/.inputrc
ln --relative --force -s .irbrc ~/.irbrc
mkdir -p ~/.config/fish/functions/
ln --relative --force -s fish_prompt.fish ~/.config/fish/functions/fish_prompt.fish

