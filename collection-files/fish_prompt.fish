function fish_prompt --description 'Write out the prompt'
	set -l last_status $status

  # User
  echo -n (whoami)
  echo -n ': '

  # PWD
  set_color green
  echo -n (pwd)

  __terlar_git_prompt
  echo

  if not test $last_status -eq 0
    set_color red
  end

  echo -n '➤ '
  set_color normal
end
